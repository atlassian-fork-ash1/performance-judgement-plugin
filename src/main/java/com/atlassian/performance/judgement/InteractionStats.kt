package com.atlassian.performance.judgement

import java.time.Duration

internal data class InteractionStats(
        val cohort: String,
        val volumes: Map<String, Long>,
        val locations: Map<String, Duration>
)
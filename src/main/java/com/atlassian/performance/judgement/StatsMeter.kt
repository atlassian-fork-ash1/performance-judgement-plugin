package com.atlassian.performance.judgement

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.maven.plugin.logging.Log
import java.time.Duration

internal data class StatsMeter(
        private val statsReader: StatsReader,
        private val cohort: String,
        private val log: Log
) {

    fun measureLocations(
            criteria: PerformanceCriteria
    ): InteractionStats {
        val interactionData = statsReader.read(criteria.getInteractionKeys())
        log.info("\n**************** $cohort ****************")
        log.info("Measuring $interactionData")
        val locations = mutableMapOf<String, Duration>()
        val volumes = mutableMapOf<String, Long>()
        criteria.getInteractionKeys().forEach { interaction ->
            val durationData = interactionData[interaction] ?: DurationData.createEmptyMilliseconds()
            log.info("Found $interaction data for $cohort")
            val location = measureLocation(durationData.stats, criteria)
            locations[interaction] = durationData.durationMapping(location)
            volumes[interaction] = durationData.stats.n
        }
        return InteractionStats(cohort, volumes, locations)
    }

    private fun measureLocation(
            data: DescriptiveStatistics,
            criteria: PerformanceCriteria
    ): Double {
        val lowerBound = Math.floor((data.n * criteria.lowerTrim).toDouble()).toInt()
        val upperBound = Math.ceil((data.n * criteria.upperTrim).toDouble()).toInt()
        val location = criteria.statistic.evaluate(data.values, lowerBound, upperBound)
        log.info("Trimmed the data by index range: [$lowerBound, $upperBound]")
        log.info("Measured location at $location")
        log.info("----------------")
        return location
    }
}
package com.atlassian.performance.judgement

internal data class MergingReader(
        val readers: Collection<StatsReader>
) : StatsReader {

    override fun read(
            interactions: Iterable<String>
    ): Map<String, DurationData> {
        val interactionData = mutableMapOf<String, DurationData>()
        readers.map { it.read(interactions) }
                .forEach { interactionData.putAll(it) }
        return interactionData
    }
}

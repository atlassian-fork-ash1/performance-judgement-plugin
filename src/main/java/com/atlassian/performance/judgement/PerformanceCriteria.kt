package com.atlassian.performance.judgement

import org.apache.commons.math3.stat.descriptive.UnivariateStatistic
import org.apache.commons.math3.stat.descriptive.moment.Mean
import java.util.*

internal data class PerformanceCriteria(
        val interactions: Iterable<Interaction>,
        val minimumVolume: Long,
        val statistic: UnivariateStatistic,
        val lowerTrim: Float,
        val upperTrim: Float
) {

    constructor(
            tolerableInteractions: Iterable<Interaction>,
            minimumVolume: Long
    ) : this(
            interactions = tolerableInteractions,
            minimumVolume = minimumVolume,
            statistic = Mean(),
            lowerTrim = 0.01f,
            upperTrim = 0.99f
    )

    fun getInteractionKeys(): Collection<String> {
        return interactions.map { it.key }
    }
}

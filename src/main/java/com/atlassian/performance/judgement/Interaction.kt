package com.atlassian.performance.judgement

data class Interaction(
        val key: String,
        val toleranceRatio: Float,
        val label: String
)